cmake_minimum_required(VERSION 3.19)
project(projekt_2)

set(CMAKE_CXX_STANDARD 98)

add_executable(projekt_2 main.cpp)