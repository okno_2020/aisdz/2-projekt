# Temat p2

Dla listy książek o polach (tytuł, cena, gatunek), zaczynającej się
pod jakimś adresem, napisać definicję struktury oraz jedną funkcję (bez definiowania i
wykorzystywania innych funkcji), która:

- drukuje tytuły książek jakiegoś gatunku
- usuwa z listy pierwszą książkę, która ma cenę zawierającą się pomiędzy dwoma jakimiś wartościami i przekazuje przez parametr jej tytuł
- zwraca (przez return) średnią cenę książek jakiegoś gatunku

Uwaga: słowo jakiś oznacza parametr funkcji
