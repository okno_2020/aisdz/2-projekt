//# Temat p2
//
//Dla listy książek o polach (tytuł, cena, gatunek), zaczynającej się
//pod jakimś adresem, napisać definicję struktury oraz jedną funkcję (bez definiowania i
//wykorzystywania innych funkcji), która:
//
//- drukuje tytuły książek jakiegoś gatunku
//- usuwa z listy pierwszą książkę, która ma cenę zawierającą się pomiędzy dwoma jakimiś wartościami i przekazuje przez parametr jej tytuł
//- zwraca (przez return) średnią cenę książek jakiegoś gatunku
//
//Uwaga: słowo jakiś oznacza parametr funkcji

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <math.h>

using namespace std;

string titles[] = {
        "Nymph Of The Land",
        "Stranger Of The Solstice",
        "Cats Without Honor",
        "Blacksmiths Of The Eclipse",
        "Vultures And Robots",
        "Strangers And Foreigners",
        "Hope Of The Sea",
        "Strife Of The Curse",
        "Call To The Hunter",
        "Prepare For My Enemies"
};

enum Genres {
    horror,
    fantasy,
    romance,
    sciFi,
    adventure,
    kids,
    education,
    travel,
    guide,
    historical
};

struct Books {
    string title;
    float price;
    Genres genre;
    Books* next;
};

string genreToString(Genres genreId) {
    switch (genreId) {
        case horror: return "horror";
        case fantasy: return "fantasy";
        case romance: return "romance";
        case sciFi: return "sciFi";
        case adventure: return "adventure";
        case kids: return "kids";
        case education: return "education";
        case travel: return "travel";
        case guide: return "guide";
        case historical: return "historical";
    }
}

void fillRandomBooks(Books *&headAddress, int qty, bool print = false) {
    Books *newTail = headAddress;
    for (int i = 0; i < qty; ++i) {
        Books *newListElement = new Books;
        newListElement->price = (float)rand() / 100 + 20;
        newListElement->genre = static_cast<Genres>(rand() % (10));
        newListElement->title = titles[rand() % (10)];
        newListElement->next = NULL;
        if (newTail == NULL) {
            headAddress = newListElement;
            newTail = newListElement;
        }
        newTail->next = newListElement;
        newTail = newTail->next;
        if (print) {
            cout << newListElement->title << ", " << genreToString(newListElement->genre) << ", $" << newListElement->price << endl;
        }
    }
}

float printGenreAndRemoveFirstFromRange(Books *&listHead, Genres genre, float minPrice, float maxPrice, string &title) {
    if(listHead == NULL || minPrice > maxPrice) {
        return (float)0;
    }

    Books *printHead = listHead, *previous = NULL;
    title = "";

    cout << "ksiazki z gatunku: " << endl;

    float avgPrice = 0;
    int avgQty = 0;

    while (printHead!=NULL) {
        if (printHead->genre == genre) {

            cout << printHead->title << ", $" << printHead->price << endl;

            avgPrice = avgPrice + printHead->price;
            avgQty++;

            if(title.empty() && printHead->price > minPrice && printHead->price < maxPrice) {
                title = printHead->title;

                Books *temp = printHead;
                if (previous == NULL) {
                    listHead = listHead->next;
                } else {
                    previous->next = printHead->next;
                }
                delete temp;
                printHead = printHead->next;
            }
        }

        previous = printHead;
        printHead = printHead->next;
    }

    return avgPrice / (float)avgQty;
}


int main() {
    srand(time(NULL));

    Books *head = NULL;
    fillRandomBooks(head, 100, false);

    string title;
    cout << endl;

    const int min = 10;
    const int max = 2000;
    const Genres selectedGenre = historical;

    cout << "wybrany gatunek: " << genreToString(selectedGenre) << endl << endl;

    float avg = printGenreAndRemoveFirstFromRange(head, selectedGenre, min, max, title);

    if (!isnan(avg)) {
        cout << endl;
        cout << "srednia cena: " << avg << endl;
        cout << "usunieta pierwsza ksiazka w cenie z przedzialu " << min << '/' << max << ": " << title;
    }

    return 0;
}
